import { MessageEmbed } from "discord.js";

let getPrefix = (message: any, prefix: string): string => {
  var userMessage: string = message.content.replace(prefix, "");
  return userMessage.trimLeft();
};

let pingUser = (message: any): void => {
  message.channel.send("Pong!").then((m: any) => {
    var ping = m.createdTimestamp - message.createdTimestamp;
    var embed = new MessageEmbed().setAuthor(`Your ping is ${ping}ms`);
    message.channel.send(embed);
  });
};

let gitStatus = (message: any): void => {
  let statusMsg = `**Changes not staged for commit:**
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
  \tmodified:   src/public/button_actions.py
  \tmodified:   src/public/buttons.py`;

  var embedStatus = new MessageEmbed()
    .setDescription(statusMsg)
    .setColor("#CE012F");
  message.channel.send(embedStatus);
};

export { getPrefix, pingUser, gitStatus };
