import { exit } from "process";
const clc = require("cli-color");

let sleep = (miliseconds: number) =>
  new Promise((resolve) => setTimeout(resolve, miliseconds));

let richPresence = (toWatch: string): object => {
  return {
    status: "online",
    activity: {
      name: ` the ${toWatch} 👀.`,
      type: "WATCHING",
    },
  };
};

let changePresence = async (bot: any, i: number) => {
  let whatToSee = [
    "Repository",
    "Issues",
    "Updates",
    "Dependencies",
    "Pull requests",
  ];
  if (i >= whatToSee.length) i = 0;

  if (bot.user !== null) {
    bot.user.setPresence(richPresence(whatToSee[i]));
  }
  await sleep(5000);
  changePresence(bot, ++i);
};

let startBot = (bot: any): void => {
  console.log("Conecting Github bot...");
  setTimeout(() => {
    if (bot.user === null) {
      console.log(clc.red("Bot connection failed."));
      exit(1);
    }
    console.clear();

    bot.user.setPresence(richPresence("Repository"));
    console.log(clc.green("Github bot now connected!"));
  }, 800);
};

export { richPresence, changePresence, startBot };
